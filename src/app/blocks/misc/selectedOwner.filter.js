(function () {
    'use strict';

    angular.module('dtag.blocks.misc')
        .filter('selectedOwner', selectedOwner)
    ;

    function selectedOwner() {
        return function (items, ownerId) {
            if (angular.isObject(ownerId) && ownerId.id) {
                // allow user to pass in a doctor object
                ownerId = ownerId.id;
            }

            ownerId = parseInt(ownerId, 10);

            if (!ownerId) {
                return items;
            }

            var filtered = [];

            angular.forEach(items, function(item) {
                if (!angular.isObject(item)) {
                    return;
                }

                try {
                    // we use item.id == null for the option to indicate 'All" at the moment
                    if (item.id == null || ownerId === item.owner.id) {
                        filtered.push(item);
                    }
                } catch (e) {}
            });

            return filtered;
        }
    }
})();