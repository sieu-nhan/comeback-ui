(function () {
    'use strict';

    angular.module('dtag.blocks.misc')
        .filter('selectedVenue', selectedVenue)
    ;

    function selectedVenue() {
        return function (items, venueId) {
            if (angular.isObject(venueId) && venueId.id) {
                // allow user to pass in a doctor object
                venueId = venueId.id;
            }

            venueId = parseInt(venueId, 10);

            if (!venueId) {
                return items;
            }

            var filtered = [];

            angular.forEach(items, function(item) {
                if (!angular.isObject(item)) {
                    return;
                }

                try {
                    // we use item.id == null for the option to indicate 'All" at the moment
                    if (item.id == null || venueId === item.venue.id) {
                        filtered.push(item);
                    }
                } catch (e) {}
            });

            return filtered;
        }
    }
})();