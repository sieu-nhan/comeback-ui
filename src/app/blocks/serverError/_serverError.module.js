(function () {
    'use strict';

    angular.module('dtag.blocks.serverError', [
        'dtag.blocks.alerts'
    ]);
})();