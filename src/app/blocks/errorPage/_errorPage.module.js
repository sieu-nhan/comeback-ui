(function () {
    'use strict';

    angular.module('dtag.blocks.errorPage', [
        'dtag.blocks.alerts'
    ]);
})();