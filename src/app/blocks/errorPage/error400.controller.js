(function () {
    'use strict';

    angular.module('dtag.blocks.errorPage')
        .controller('400ErrorController', function(AlertService, $translate) {
            AlertService.replaceAlerts({
                type: 'error',
                message: $translate.instant('ERROR_PAGE.400')
            });
        })
    ;
})();