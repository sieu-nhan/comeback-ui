window.deferredBootstrapper.bootstrap({
    element: document,
    module: 'dtag',
    injectorModules: ['dtag.core.bootstrap', 'dtag.core.auth'],
    moduleResolves: [
        {
            module: 'dtag.core.bootstrap',
            resolve: {
                EXISTING_SESSION: ['Auth', function (Auth) {
                    return Auth.check().catch(function () {
                        return false;
                    }).finally(function () {
                        console.log('auth resolved');
                    });
                }]
            }
        }
    ]
});