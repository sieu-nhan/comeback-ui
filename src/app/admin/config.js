(function () {
    'use strict';

    angular
        .module('dtag.admin')
        .constant('CITY', [
            {code: 'AG', key: 'An Giang', label: 'An Giang'},
            {code: 'BD', key: 'Bình Dương', label: 'Bình Dương'},
            {code: 'BĐ', key: 'Bình Định', label: 'Bình Định'},
            {code: 'BG', key: 'Bắc Giang', label: 'Bắc Giang'},
            {code: 'BK', key: 'Bắc Kạn', label: 'Bắc Kạn'},
            {code: 'BL', key: 'Bạc Liêu', label: 'Bạc Liêu'},
            {code: 'BN', key: 'Bắc Ninh', label: 'Bắc Ninh'},
            {code: 'BP', key: 'Bình Phước', label: 'Bình Phước'},
            {code: 'BR', key: 'Bà Rịa - Vũng Tàu', label: 'Bà Rịa - Vũng Tàu'},
            {code: 'BT', key: 'Bình Thuận', label: 'Bình Thuận'},
            {code: 'BTR', key: 'Bến Tre', label: 'Bến Tre'},
            {code: 'CB', key: 'Cao Bằng', label: 'Cao Bằng'},
            {code: 'CM', key: 'Cà Mau', label: 'Cà Mau'},
            {code: 'CT', key: 'TP Cần Thơ', label: 'TP Cần Thơ'},
            {code: 'ĐB', key: 'Điện Biên', label: 'Điện Biên'},
            {code: 'ĐL', key: 'Đắk Lắk', label: 'Đắk Lắk'},
            {code: 'ĐN', key: 'TP Đà Nẵng', label: 'TP Đà Nẵng'},
            {code: 'ĐN', key: 'Đồng Nai', label: 'Đồng Nai'},
            {code: 'ĐNO', key: 'Đắk Nông', label: 'Đắk Nông'},
            {code: 'ĐT', key: 'Đồng Tháp', label: 'Đồng Tháp'},
            {code: 'GL', key: 'Gia Lai', label: 'Gia Lai'},
            {code: 'HB', key: 'Hòa Bình', label: 'Hòa Bình'},
            {code: 'HCM', key: 'TP Hồ Chí Minh', label: 'TP Hồ Chí Minh'},
            {code: 'HD', key: 'Hải Dương', label: 'Hải Dương'},
            {code: 'HG', key: 'Hậu Giang', label: 'Hậu Giang'},
            {code: 'HGI', key: 'Hà Giang', label: 'Hà Giang'},
            {code: 'HN', key: 'Hà Nội', label: 'Hà Nội'},
            {code: 'HN', key: 'Hà Nam', label: 'Hà Nam'},
            {code: 'HP', key: 'TP Hải Phòng', label: 'TP Hải Phòng'},
            {code: 'HT', key: 'Hà Tĩnh', label: 'Hà Tĩnh'},
            {code: 'HY', key: 'Hưng Yên', label: 'Hưng Yên'},
            {code: 'KG', key: 'Kiên Giang', label: 'Kiên Giang'},
            {code: 'KH', key: 'Khánh Hòa', label: 'Khánh Hòa'},
            {code: 'KT', key: 'Kon Tum', label: 'Kon Tum'},
            {code: 'LA', key: 'Long An', label: 'Long An'},
            {code: 'LC', key: 'Lào Cai', label: 'Lào Cai'},
            {code: 'LCH', key: 'Lai Châu', label: 'Lai Châu'},
            {code: 'LĐ', key: 'Lâm Đồng', label: 'Lâm Đồng'},
            {code: 'LS', key: 'Lạng Sơn', label: 'Lạng Sơn'},
            {code: 'NA', key: 'Nghệ An', label: 'Nghệ An'},
            {code: 'NB', key: 'Ninh Bình', label: 'Ninh Bình'},
            {code: 'NĐ', key: 'Nam Định', label: 'Nam Định'},
            {code: 'NT', key: 'Ninh Thuận', label: 'Ninh Thuận'},
            {code: 'PT', key: 'Phú Thọ', label: 'Phú Thọ'},
            {code: 'PY', key: 'Phú Yên', label: 'Phú Yên'},
            {code: 'QB', key: 'Quảng Bình', label: 'Quảng Bình'},
            {code: 'QN', key: 'Quảng Ninh', label: 'Quảng Ninh'},
            {code: 'QN', key: 'Quảng Ninh', label: 'Quảng Ninh'},
            {code: 'QNA', key: 'Quảng Nam', label: 'Quảng Nam'},
            {code: 'QNG', key: 'Quảng Ngãi', label: 'Quảng Ngãi'},
            {code: 'QT', key: 'Quảng Trị', label: 'Quảng Trị'},
            {code: 'SL', key: 'Sơn La', label: 'Sơn La'},
            {code: 'ST', key: 'Sóc Trăng', label: 'Sóc Trăng'},
            {code: 'TB', key: 'Thái Bình', label: 'Thái Bình'},
            {code: 'TG', key: 'Tiền Giang', label: 'Tiền Giang'},
            {code: 'TH', key: 'Thanh Hóa', label: 'Thanh Hóa'},
            {code: 'TN', key: 'Thái Nguyên', label: 'Thái Nguyên'},
            {code: 'TNI', key: 'Tây Ninh', label: 'Tây Ninh'},
            {code: 'TQ', key: 'Tuyên Quang', label: 'Tuyên Quang'},
            {code: 'TT', key: 'Thừa Thiên - Huế', label: 'Thừa Thiên - Huế'},
            {code: 'TV', key: 'Trà Vinh', label: 'Trà Vinh'},
            {code: 'VL', key: 'Vĩnh Long', label: 'Vĩnh Long'},
            {code: 'VP', key: 'Vĩnh Phúc', label: 'Vĩnh Phúc'},
            {code: 'YB', key: 'Yên Bái', label: 'Yên Bái'}
        ])
        .provider('API_ADMIN_BASE_URL', {
            $get: function(API_END_POINT) {
                return API_END_POINT + '/admin/v1';
            }
        })
    ;

})();