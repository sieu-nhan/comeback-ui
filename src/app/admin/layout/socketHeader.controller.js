(function () {
    'use strict';

    angular
        .module('dtag.admin.layout')
        .controller('SocketHeader', SocketHeader)
    ;

    function SocketHeader($scope, $state, $translate, $interpolate, $element, $window, _, CONNECT_SOCKET, TOTAL_STORAGE, LIST_ORDER) {
        $window.localStorage[LIST_ORDER] = $window.localStorage[LIST_ORDER] || '[]';
        $scope.newOrders = angular.fromJson($window.localStorage[LIST_ORDER]);
        $scope.totalOrder = $scope.newOrders.length;

        var socket = io.connect(CONNECT_SOCKET);
        socket.on('notification', function (data) {
            $scope.$apply(function() {
                $scope.newOrders.push(data);
                $scope.totalOrder++;

                setTotalOrderStorage();
            });
        });

        $scope.hasView = function(order) {
            var index = _.findIndex($scope.newOrders, function(newOrder) {
                return newOrder.id == order.id
            });

            if(index > -1) {
                $scope.newOrders.splice(index, 1);
                $scope.totalOrder--;

                setTotalOrderStorage();
            }
        };

        function setTotalOrderStorage() {
            $window.localStorage[TOTAL_STORAGE] = $scope.totalOrder;
            $window.localStorage[LIST_ORDER] = angular.toJson($scope.newOrders);

            updateTitle();
        }

        var updateTitle = function() {
            var pageTitle = '';
            var state = $state.$current;

            if(_.isObject(state.ncyBreadcrumb) && state.ncyBreadcrumb.label) {
                var title = state.ncyBreadcrumb.label;
                pageTitle = $interpolate(title)(state.locals.globals);
            }

            if(!!$window.localStorage[TOTAL_STORAGE] && Number($window.localStorage[TOTAL_STORAGE]) > 0) {
                pageTitle = '(' + $window.localStorage[TOTAL_STORAGE] + ') ' + pageTitle;
            }

            $window.document.title = pageTitle;
        };
    }
})();