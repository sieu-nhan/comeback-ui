(function() {
    'use strict';

    angular.module('dtag.admin.usersManagement.customer')
        .controller('CustomerForm', CustomerForm)
    ;

    function CustomerForm($scope, customerRestangular, AlertService, ServerErrorProcessor, historyStorage, customer, provinces, HISTORY_TYPE_PATH) {
        $scope.fieldNameTranslations = {
            username: 'Username',
            plainPassword: 'Password'
        };

        $scope.isNew = customer === null;
        $scope.formProcessing = false;
        $scope.provinces = provinces;
        $scope.districts = [];
        $scope.selectData = {
            province: null
        };

        $scope.customer = customer || {
            username: null,
            email: null,
            enabled: true,
            name: null,
            phone: null,
            address: null,
            gender: false,
            district: null,
            bankName: null,
            branch: null,
            accountOwner: null,
            accountNumber: null
        };

        if($scope.provinces.length == 1) {
            $scope.selectData = {
                province: $scope.provinces[0].id
            };

            angular.forEach($scope.provinces[0].districts, function(district) {
                if(district.type == 1) {
                    $scope.districts.push(district);
                }
            });
        }

        $scope.backToListCustomer = function() {
            return historyStorage.getLocationPath(HISTORY_TYPE_PATH.customer, '^.list');
        };

        $scope.isFormValid = function() {
            return $scope.userForm.$valid;
        };

        $scope.selectProvince = function(province) {
            $scope.districts = [];

            angular.forEach(province.districts, function(district) {
                if(district.type == 1) {
                    $scope.districts.push(district);
                }
            });
        };

        $scope.submit = function() {
            if ($scope.formProcessing) {
                // already running, prevent duplicates
                return;
            }

            $scope.formProcessing = true;

            delete $scope.customer.lastLogin;
            delete $scope.customer.roles;

            var saveUser = $scope.isNew ? customerRestangular.one('customers').post(null, $scope.customer) : $scope.customer.patch();

            saveUser
                .catch(
                function (response) {
                    var errorCheck = ServerErrorProcessor.setFormValidationErrors(response, $scope.userForm, $scope.fieldNameTranslations);
                    $scope.formProcessing = false;

                    return errorCheck;
                })
                .then(
                function () {
                    AlertService.addFlash({
                        type: 'success',
                        message: 'Thêm khách hàng thành công'
                    });
                })
                .then(
                function () {
                    return historyStorage.getLocationPath(HISTORY_TYPE_PATH.customer, '^.list');
                })
            ;
        };
    }
})();