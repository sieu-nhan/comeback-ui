(function() {
    'use strict';

    angular.module('dtag.admin.usersManagement.customer')
        .controller('CustomerList', CustomerList)
    ;

    function CustomerList($scope, customers, AlertService, adminUserManager, autoLogin, historyStorage, HISTORY_TYPE_PATH) {
        $scope.customers = customers;

        $scope.hasData = function () {
            return !!customers.length;
        };

        if (!$scope.hasData()) {
            AlertService.replaceAlerts({
                type: 'warning',
                message: 'Hiện không có khách hàng nào'
            });
        }

        $scope.showPagination = showPagination;
        $scope.toggleManagerStatus = toggleManagerStatus;
        $scope.visitManager = visitManager;

        $scope.tableConfig = {
            itemsPerPage: 10,
            maxPages: 10
        };

        function toggleManagerStatus(customer) {
            var newStatus = !customer.enabled;
            var isPause = !newStatus;

            return customerRestangular.one('customers', customer.id).patch({ 'enabled': newStatus })
                .then(function () {
                    customer.enabled = newStatus;

                    var successMessage;

                    if (isPause) {
                        successMessage = 'Khách hàng đã bị cấm';
                    } else {
                        successMessage = 'Khách hàng đã được cho phép sử dụng';
                    }

                    AlertService.replaceAlerts({
                        type: 'success',
                        message: successMessage
                    });
                })
                .catch(function () {
                    AlertService.replaceAlerts({
                        type: 'error',
                        message: 'Cập nhật thất bại'
                    });
                })

                ;
        }

        function showPagination() {
            return angular.isArray($scope.managers) && $scope.managers.length > $scope.tableConfig.itemsPerPage;
        }

        function visitManager(publisherId) {
            adminUserManager.one(publisherId).one('token').get()
                .then(function(tokenPublisher) {
                    autoLogin.switchToUser(tokenPublisher.plain(), 'app.customer.dashboard');
                });
        }

        $scope.$on('$locationChangeSuccess', function() {
            historyStorage.setParamsHistoryCurrent(HISTORY_TYPE_PATH.customer)
        });
    }
})();