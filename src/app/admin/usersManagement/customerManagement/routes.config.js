(function() {
    'use strict';

    angular.module('dtag.admin.usersManagement.customer')
        .config(addStates)
    ;

    function addStates(UserStateHelperProvider) {
        UserStateHelperProvider.state('usersManagement.customer', {
                abstract: true,
                url: '/customers',
                ncyBreadcrumb: {
                    skip: true
                }
            })
            .state('usersManagement.customer.list', {
                url: '/list?page&sortField&orderBy&search',
                views: {
                    'content@app': {
                        controller: 'CustomerList',
                        templateUrl: 'admin/usersManagement/customerManagement/customerList.tpl.html'
                    }
                },
                resolve: {
                    customers: function(customerRestangular) {
                        return customerRestangular.one('customers', 'all').getList();
                    }
                },
                ncyBreadcrumb: {
                    label: 'Danh sách khách hàng'
                },
                reloadOnSearch: false
            })

            .state('usersManagement.customer.new', {
                url: '/new',
                views: {
                    'content@app': {
                        controller: 'CustomerForm',
                        templateUrl: 'admin/usersManagement/customerManagement/customerForm.tpl.html'
                    }
                },
                resolve: {
                    customer : function() {
                        return null;
                    },
                    provinces: function(ProvinceManager) {
                        return ProvinceManager.getList();
                    }
                },
                ncyBreadcrumb: {
                    label: 'Thêm khách hàng'
                }
            })

            .state({
                name: 'usersManagement.customer.edit',
                url: '/edit/{id:[0-9]+}',
                views: {
                    'content@app': {
                        controller: 'CustomerForm',
                        templateUrl: 'admin/usersManagement/customerManagement/customerForm.tpl.html'
                    }
                },
                resolve: {
                    customer: function(customerRestangular, $stateParams) {
                        return customerRestangular.one('customers', $stateParams.id).get();
                    },
                    provinces: function(ProvinceManager) {
                        return ProvinceManager.getList();
                    }
                },
                ncyBreadcrumb: {
                    label: 'Sửa khách hàng'
                }
            })
        ;
    }
})();