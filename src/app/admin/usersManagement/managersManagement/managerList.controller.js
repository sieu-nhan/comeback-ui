(function() {
    'use strict';

    angular.module('dtag.admin.usersManagement.managers')
        .controller('ManagerList', ManagerList)
    ;

    function ManagerList($scope, $translate, managers, AlertService, managerRestangular, adminUserManager, autoLogin, historyStorage, HISTORY_TYPE_PATH) {
        $scope.managers = managers;

        $scope.hasData = function () {
            return !!managers.length;
        };

        if (!$scope.hasData()) {
            AlertService.replaceAlerts({
                type: 'warning',
                message: $translate.instant('MANAGER_MODULE.CURRENTLY_NO_MANAGER')
            });
        }

        $scope.showPagination = showPagination;
        $scope.toggleManagerStatus = toggleManagerStatus;
        $scope.visitManager = visitManager;

        $scope.tableConfig = {
            itemsPerPage: 10,
            maxPages: 10
        };

        function toggleManagerStatus(manager) {
            var newStatus = !manager.enabled;
            var isPause = !newStatus;

            return managerRestangular.one('managers', manager.id).patch({ 'enabled': newStatus })
                .then(function () {
                    manager.enabled = newStatus;

                    var successMessage;

                    if (isPause) {
                        successMessage = $translate.instant('MANAGER_MODULE.PAUSE_STATUS_SUCCESS');
                    } else {
                        successMessage = $translate.instant('MANAGER_MODULE.ACTIVE_STATUS_SUCCESS');
                    }

                    AlertService.replaceAlerts({
                        type: 'success',
                        message: successMessage
                    });
                })
                .catch(function () {
                    AlertService.replaceAlerts({
                        type: 'error',
                        message: $translate.instant('MANAGER_MODULE.UPDATE_STATUS_FAIL')
                    });
                })

                ;
        }

        function showPagination() {
            return angular.isArray($scope.managers) && $scope.managers.length > $scope.tableConfig.itemsPerPage;
        }

        function visitManager(publisherId) {
            adminUserManager.one(publisherId).one('token').get()
                .then(function(tokenPublisher) {
                    autoLogin.switchToUser(tokenPublisher.plain(), 'app.manager.dashboard');
                });
        }

        $scope.$on('$locationChangeSuccess', function() {
            historyStorage.setParamsHistoryCurrent(HISTORY_TYPE_PATH.manager)
        });
    }
})();