(function() {
    'use strict';

    angular.module('dtag.admin.usersManagement.managers')
        .config(addStates)
    ;

    function addStates(UserStateHelperProvider) {
        UserStateHelperProvider.state('usersManagement.managers', {
                abstract: true,
                url: '/managers',
                ncyBreadcrumb: {
                    skip: true
                }
            })
            .state('usersManagement.managers.list', {
                url: '/list?page&sortField&orderBy&search',
                views: {
                    'content@app': {
                        controller: 'ManagerList',
                        templateUrl: 'admin/usersManagement/managersManagement/managerList.tpl.html'
                    }
                },
                resolve: {
                    managers: function(managerRestangular) {
                        return managerRestangular.one('managers').getList();
                    }
                },
                ncyBreadcrumb: {
                    label: '{{ "NAV_MODULE.MANAGERS" | translate }}'
                },
                reloadOnSearch: false
            })

            .state('usersManagement.managers.new', {
                url: '/new',
                views: {
                    'content@app': {
                        controller: 'ManagerForm',
                        templateUrl: 'admin/usersManagement/managersManagement/managerForm.tpl.html'
                    }
                },
                resolve: {
                    manager : function() {
                        return null;
                    }
                },
                ncyBreadcrumb: {
                    label: '{{ "MANAGER_MODULE.NEW_MANAGER" | translate }}'
                }
            })

            .state({
                name: 'usersManagement.managers.edit',
                url: '/edit/{id:[0-9]+}',
                views: {
                    'content@app': {
                        controller: 'ManagerForm',
                        templateUrl: 'admin/usersManagement/managersManagement/managerForm.tpl.html'
                    }
                },
                resolve: {
                    manager: function($stateParams, managerRestangular) {
                        return managerRestangular.one('managers', $stateParams.id).get();
                    }
                },
                ncyBreadcrumb: {
                    label: '{{ "MANAGER_MODULE.EDIT_MANAGER" | translate }} - {{ manager.username }}'
                }
            })
        ;
    }
})();