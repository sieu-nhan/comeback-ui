(function() {
    'use strict';

    angular.module('dtag.admin.usersManagement')
        .config(addStates)
    ;

    function addStates(UserStateHelperProvider) {
        UserStateHelperProvider
            .state('usersManagement', {
                abstract: true,
                url: '/usersManagement',
                ncyBreadcrumb: {
                    label: 'Users Management'
                }
            })
        ;
    }
})();