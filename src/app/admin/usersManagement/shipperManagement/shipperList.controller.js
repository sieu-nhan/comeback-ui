(function() {
    'use strict';

    angular.module('dtag.admin.usersManagement.shippers')
        .controller('ShipperList', ShipperList)
    ;

    function ShipperList($scope, $translate, shippers, AlertService, ShipperRestangular, adminUserManager, autoLogin, historyStorage, HISTORY_TYPE_PATH) {
        $scope.shippers = shippers;

        $scope.hasData = function () {
            return !!shippers.length;
        };

        if (!$scope.hasData()) {
            AlertService.replaceAlerts({
                type: 'warning',
                message: $translate.instant('SHIPPER_MODULE.CURRENTLY_NO_SHIPPER')
            });
        }

        $scope.showPagination = showPagination;
        $scope.toggleManagerStatus = toggleManagerStatus;
        $scope.visitManager = visitManager;

        $scope.tableConfig = {
            itemsPerPage: 10,
            maxPages: 10
        };

        function toggleManagerStatus(shipper) {
            var newStatus = !shipper.enabled;
            var isPause = !newStatus;

            return ShipperRestangular.one('shippers', shipper.id).patch({ 'enabled': newStatus })
                .then(function () {
                    shipper.enabled = newStatus;

                    var successMessage;

                    if (isPause) {
                        successMessage = $translate.instant('SHIPPER_MODULE.PAUSE_STATUS_SUCCESS');
                    } else {
                        successMessage = $translate.instant('SHIPPER_MODULE.ACTIVE_STATUS_SUCCESS');
                    }

                    AlertService.replaceAlerts({
                        type: 'success',
                        message: successMessage
                    });
                })
                .catch(function () {
                    AlertService.replaceAlerts({
                        type: 'error',
                        message: $translate.instant('SHIPPER_MODULE.UPDATE_STATUS_FAIL')
                    });
                })

                ;
        }

        function showPagination() {
            return angular.isArray($scope.shippers) && $scope.shippers.length > $scope.tableConfig.itemsPerPage;
        }

        function visitManager(publisherId) {
            adminUserManager.one(publisherId).one('token').get()
                .then(function(tokenPublisher) {
                    autoLogin.switchToUser(tokenPublisher.plain(), 'app.manager.dashboard');
                });
        }

        $scope.$on('$locationChangeSuccess', function() {
            historyStorage.setParamsHistoryCurrent(HISTORY_TYPE_PATH.manager)
        });
    }
})();