(function() {
    'use strict';

    angular.module('dtag.admin.usersManagement.shippers')
        .config(addStates)
    ;

    function addStates(UserStateHelperProvider) {
        UserStateHelperProvider.state('usersManagement.shippers', {
                abstract: true,
                url: '/managers',
                ncyBreadcrumb: {
                    skip: true
                }
            })
            .state('usersManagement.shippers.list', {
                url: '/list?page&sortField&orderBy&search',
                views: {
                    'content@app': {
                        controller: 'ShipperList',
                        templateUrl: 'admin/usersManagement/shipperManagement/shipperList.tpl.html'
                    }
                },
                resolve: {
                    shippers: function(ShipperRestangular) {
                        return ShipperRestangular.one('shippers').getList();
                    }
                },
                ncyBreadcrumb: {
                    label: 'Danh sách shipper'
                },
                reloadOnSearch: false
            })

            .state('usersManagement.shippers.new', {
                url: '/new',
                views: {
                    'content@app': {
                        controller: 'ShipperForm',
                        templateUrl: 'admin/usersManagement/shipperManagement/shipperForm.tpl.html'
                    }
                },
                resolve: {
                    shipper : function() {
                        return null;
                    }
                },
                ncyBreadcrumb: {
                    label: 'Thêm mới shipper'
                }
            })

            .state({
                name: 'usersManagement.shippers.edit',
                url: '/edit/{id:[0-9]+}',
                views: {
                    'content@app': {
                        controller: 'ShipperForm',
                        templateUrl: 'admin/usersManagement/shipperManagement/shipperForm.tpl.html'
                    }
                },
                resolve: {
                    shipper: function($stateParams, ShipperRestangular) {
                        return ShipperRestangular.one('shippers', $stateParams.id).get();
                    }
                },
                ncyBreadcrumb: {
                    label: 'Sửa thông tin shipper - {{ shipper.username }}'
                }
            })
        ;
    }
})();