(function() {
    'use strict';

    angular.module('dtag.admin.usersManagement.shippers')
        .controller('ShipperForm', ShipperForm)
    ;

    function ShipperForm($scope, $translate, ShipperRestangular, AlertService, ServerErrorProcessor, historyStorage, shipper, CITY, HISTORY_TYPE_PATH) {
        $scope.fieldNameTranslations = {
            username: 'Username',
            plainPassword: 'Password'
        };

        $scope.isNew = shipper === null;
        $scope.formProcessing = false;

        $scope.city = CITY;
        $scope.shipper = shipper || {
            username: null,
            email: null,
            enabled: true,
            name: null,
            phone: null,
            address: null,
            gender: false
        };

        /**
         *
         * @param {Array} data
         * @param {String} [label]
         * @returns {Array}
         */
        function addAllOption(data, label)
        {
            if (!angular.isArray(data)) {
                throw new Error('Expected an array of data');
            }

            data.unshift({
                id: null, // default value
                name: label || 'All'
            });

            return data;
        }

        $scope.groupEntities = function (item){
            if (item.id === null) {
                return undefined; // no group
            }

            return ''; // separate group with no name
        };

        $scope.backToListShipper = function() {
            return historyStorage.getLocationPath(HISTORY_TYPE_PATH.shipper, '^.list');
        };

        $scope.isFormValid = function() {
            return $scope.userForm.$valid;
        };

        $scope.submit = function() {
            if ($scope.formProcessing) {
                // already running, prevent duplicates
                return;
            }

            $scope.formProcessing = true;

            delete $scope.shipper.lastLogin;
            delete $scope.shipper.roles;

            var saveUser = $scope.isNew ? ShipperRestangular.one('shippers').post(null, $scope.shipper) : $scope.shipper.patch();

            saveUser
                .catch(
                function (response) {
                    var errorCheck = ServerErrorProcessor.setFormValidationErrors(response, $scope.userForm, $scope.fieldNameTranslations);
                    $scope.formProcessing = false;

                    return errorCheck;
                })
                .then(
                function () {
                    AlertService.addFlash({
                        type: 'success',
                        message: $translate.instant('SHIPPER_MODULE.ADD_NEW_SUCCESS')
                    });
                })
                .then(
                function () {
                    return historyStorage.getLocationPath(HISTORY_TYPE_PATH.shipper, '^.list');
                })
            ;
        };
    }
})();