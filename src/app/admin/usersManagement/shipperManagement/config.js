(function () {
    'use strict';

    angular
        .module('dtag.admin.usersManagement.shippers')
        .provider('API_SHIPPER_BASE_URL', {
            $get: function(API_END_POINT) {
                return API_END_POINT + '/shipper/v1';
            }
        })
    ;

})();