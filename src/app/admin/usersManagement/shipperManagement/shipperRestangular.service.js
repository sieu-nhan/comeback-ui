(function () {
    'use strict';

    angular
        .module('dtag.admin.usersManagement.shippers')
        .factory('ShipperRestangular', ShipperRestangular)
    ;

    function ShipperRestangular(Restangular, API_SHIPPER_BASE_URL) {
        return Restangular.withConfig(function (RestangularConfigurer) {
            RestangularConfigurer.setBaseUrl(API_SHIPPER_BASE_URL);
        });
    }

})();