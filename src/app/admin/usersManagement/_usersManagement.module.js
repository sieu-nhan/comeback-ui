(function() {
    'use strict';

    angular.module('dtag.admin.usersManagement', [
        'dtag.admin.usersManagement.managers',
        'dtag.admin.usersManagement.customer',
        'dtag.admin.usersManagement.shippers'
    ]);
})();