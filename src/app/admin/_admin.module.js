(function () {
    'use strict';

    angular.module('dtag.admin', [
        'dtag.admin.layout',
        'dtag.admin.usersManagement'
    ]);
})();