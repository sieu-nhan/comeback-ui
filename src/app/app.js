angular.module('dtag', [
    'dtag.core',

    'dtag.admin',
    'dtag.manager',
    'dtag.customer',

    'dtag.management',
    'dtag.dashboard'
]);