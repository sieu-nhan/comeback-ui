(function () {
    'use strict';

    angular
        .module('dtag.manager')
        .factory('managerRestangular', managerRestangular)
    ;

    function managerRestangular(Restangular, API_MANAGER_BASE_URL) {
        return Restangular.withConfig(function (RestangularConfigurer) {
            RestangularConfigurer.setBaseUrl(API_MANAGER_BASE_URL);
        });
    }

})();