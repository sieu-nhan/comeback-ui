(function () {
    'use strict';

    angular
        .module('dtag.manager')
        .provider('API_MANAGER_BASE_URL', {
            $get: function(API_END_POINT) {
                return API_END_POINT + '/manager/v1';
            }
        })
    ;

})();