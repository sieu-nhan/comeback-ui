(function () {
    'use strict';

    angular
        .module('dtag.manager.managerManagement')
        .controller('ManagerAccountForm', ManagerAccountForm)
    ;

    function ManagerAccountForm($scope, $translate, $state, AlertService, ServerErrorProcessor, manager) {
        $scope.fieldNameTranslations = {
            username: 'Username',
            plainPassword: 'Password'
        };

        $scope.formProcessing = false;
        $scope.manager = manager;

        $scope.isFormValid = function() {
            if($scope.manager.plainPassword != null || $scope.repeatPassword != null) {
                return $scope.userForm.$valid && $scope.repeatPassword == $scope.manager.plainPassword;
            }

            return $scope.userForm.$valid;
        };

        $scope.submit = function() {
            if ($scope.formProcessing) {
                // already running, prevent duplicates
                return;
            }

            $scope.formProcessing = true;

            delete $scope.manager.enabled;
            delete $scope.manager.lastLogin;
            delete $scope.manager.owner;
            delete $scope.manager.roles;

            var saveUser = $scope.manager.patch();
            saveUser
                .catch(
                function (response) {
                    var errorCheck = ServerErrorProcessor.setFormValidationErrors(response, $scope.userForm, $scope.fieldNameTranslations);
                    $scope.formProcessing = false;

                    return errorCheck;
                })
                .then(
                function () {
                    AlertService.addFlash({
                        type: 'success',
                        message: $translate.instant('MANAGER_MODULE.UPDATE_PROFILE_SUCCESS')
                    });
                })
                .then(
                function () {
                    return $state.reload();
                })
            ;
        };
    }
})();