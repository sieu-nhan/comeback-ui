(function () {
    'use strict';

    angular
        .module('dtag.manager.managerManagement')
        .config(addStates)
    ;

    function addStates($stateProvider) {
        $stateProvider
            .state({
                name: 'app.manager.managerManagement',
                abstract: true,
                url: '/managerManagement'
            })

            .state({
                name: 'app.manager.managerManagement.edit',
                url: '/edit',
                views: {
                    'content@app': {
                        controller: 'ManagerAccountForm',
                        templateUrl: 'manager/managerManagement/managerForm.tpl.html'
                    }
                },
                resolve: {
                    manager: function(managerRestangular, userSession) {
                        return managerRestangular.one('managers', userSession.id).get();
                    }
                },
                ncyBreadcrumb: {
                    label: '{{ "EDIT_PROFILE" | translate }}'
                }
            })
        ;
    }
})();