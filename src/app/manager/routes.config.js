(function () {
    'use strict';

    angular
        .module('dtag.manager')
        .config(addStates)
    ;

    function addStates($stateProvider, USER_ROLES) {
        $stateProvider
            .state('app.manager', {
                abstract: true,
                views: {
                    'header@app': {
                        templateUrl: 'manager/layout/header.tpl.html'
                    },
                    'nav@app': {
                        templateUrl: 'manager/layout/nav.tpl.html'
                    }
                },
                url: '/manager',
                data: {
                    requiredUserRole: USER_ROLES.manager
                },
                ncyBreadcrumb: {
                    skip: true
                }
            })
            .state('app.manager.error', {
                abstract: true,
                url: '/error'
            })
            .state('app.manager.error.404', {
                url: '/404',
                views: {
                    'content@app': {
                        controller: '404ErrorController'
                    }
                },
                ncyBreadcrumb: {
                    label: '404'
                }
            })
            .state('app.manager.error.403', {
                url: '/403',
                views: {
                    'content@app': {
                        controller: '403ErrorController'
                    }
                },
                ncyBreadcrumb: {
                    label: '403'
                }
            })
            .state('app.manager.error.400', {
                url: '/400',
                views: {
                    'content@app': {
                        controller: '400ErrorController'
                    }
                },
                ncyBreadcrumb: {
                    label: '400'
                }
            })
            .state('app.manager.error.500', {
                url: '/500',
                views: {
                    'content@app': {
                        controller: '500ErrorController'
                    }
                },
                ncyBreadcrumb: {
                    label: '500'
                }
            })
        ;
    }
})();