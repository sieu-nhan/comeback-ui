(function() {
    'use strict';

    angular.module('dtag.manager', [
        'dtag.manager.layout',
        'dtag.manager.managerManagement'
    ]);
})();