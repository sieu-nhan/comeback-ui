(function () {
    'use strict';

    angular
        .module('dtag.customer.customerManagement')
        .config(addStates)
    ;

    function addStates($stateProvider) {
        $stateProvider
            .state({
                name: 'app.customer.customerManagement',
                abstract: true,
                url: '/customerManagement'
            })

            .state({
                name: 'app.customer.customerManagement.edit',
                url: '/edit',
                views: {
                    'content@app': {
                        controller: 'CustomerAccountForm',
                        templateUrl: 'customer/customerManagement/customerForm.tpl.html'
                    }
                },
                resolve: {
                    customer: function(customerRestangular) {
                        return customerRestangular.one('customers', 'current').get();
                    },
                    provinces: function(ProvinceManager) {
                        return ProvinceManager.getList();
                    }
                },
                ncyBreadcrumb: {
                    label: '{{ "EDIT_PROFILE" | translate }}'
                }
            })
        ;
    }
})();