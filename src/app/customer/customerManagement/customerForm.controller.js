(function () {
    'use strict';

    angular
        .module('dtag.customer.customerManagement')
        .controller('CustomerAccountForm', CustomerAccountForm)
    ;

    function CustomerAccountForm($scope, $translate, provinces, $state, AlertService, ServerErrorProcessor, customer) {
        $scope.fieldNameTranslations = {
            username: 'Username',
            plainPassword: 'Password'
        };

        $scope.formProcessing = false;
        $scope.customer = customer;
        $scope.provinces = provinces;
        $scope.districts = [];
        $scope.selectData = {
            province: null
        };

        if($scope.provinces.length == 1) {
            $scope.selectData = {
                province: $scope.provinces[0].id
            };

            angular.forEach($scope.provinces[0].districts, function(district) {
                if(district.type == 1) {
                    $scope.districts.push(district);
                }
            });
        }

        $scope.isFormValid = function() {
            if($scope.customer.password != null || $scope.repeatPassword != null) {
                return $scope.userForm.$valid && $scope.repeatPassword == $scope.customer.password;
            }

            return $scope.userForm.$valid;
        };

        $scope.selectProvince = function(province) {
            $scope.districts = [];

            angular.forEach(province.districts, function(district) {
                if(district.type == 1) {
                    $scope.districts.push(district);
                }
            });
        };

        $scope.submit = function() {
            if ($scope.formProcessing) {
                // already running, prevent duplicates
                return;
            }

            $scope.formProcessing = true;

            delete $scope.customer.enabled;
            delete $scope.customer.lastLogin;
            delete $scope.customer.owner;
            delete $scope.customer.roles;
            delete $scope.customer.avatar;

            var saveUser = $scope.customer.patch();
            saveUser
                .catch(
                function (response) {
                    var errorCheck = ServerErrorProcessor.setFormValidationErrors(response, $scope.userForm, $scope.fieldNameTranslations);
                    $scope.formProcessing = false;

                    return errorCheck;
                })
                .then(
                function () {
                    AlertService.addFlash({
                        type: 'success',
                        message: 'Sửa thông tin thành công'
                    });
                })
                .then(
                function () {
                    return $state.reload();
                })
            ;
        };
    }
})();