(function() {
    'use strict';

    angular.module('dtag.customer', [
        'dtag.customer.layout',
        'dtag.customer.customerManagement'
    ]);
})();