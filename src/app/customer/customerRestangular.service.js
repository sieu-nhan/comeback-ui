(function () {
    'use strict';

    angular
        .module('dtag.customer')
        .factory('customerRestangular', customerRestangular)
    ;

    function customerRestangular(Restangular, API_CUSTOMER_BASE_URL) {
        return Restangular.withConfig(function (RestangularConfigurer) {
            RestangularConfigurer.setBaseUrl(API_CUSTOMER_BASE_URL);
        });
    }

})();