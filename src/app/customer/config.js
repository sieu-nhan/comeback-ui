(function () {
    'use strict';

    angular
        .module('dtag.customer')
        .provider('API_CUSTOMER_BASE_URL', {
            $get: function(API_END_POINT) {
                return API_END_POINT + '/customer/v1';
            }
        })
    ;

})();