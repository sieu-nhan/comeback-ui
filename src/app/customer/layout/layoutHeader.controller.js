(function () {
    'use strict';

    angular
        .module('dtag.customer')
        .controller('LayoutHeader', LayoutHeader)
    ;

    function LayoutHeader($scope, autoLogin) {
        $scope.switchBackToAdminAccount = switchBackToAdminAccount;
        $scope.showButtonSwitchBackToAdmin = showButtonSwitchBackToAdmin;

        function switchBackToAdminAccount() {
            autoLogin.switchBackMyAccount('app.admin.usersManagement.customer.list');
        }

        function showButtonSwitchBackToAdmin() {
            return autoLogin.showButtonSwitchBack();
        }
    }
})();