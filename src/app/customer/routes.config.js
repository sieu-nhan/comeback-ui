(function () {
    'use strict';

    angular
        .module('dtag.customer')
        .config(addStates)
    ;

    function addStates($stateProvider, USER_ROLES) {
        $stateProvider
            .state('app.customer', {
                abstract: true,
                views: {
                    'header@app': {
                        templateUrl: 'customer/layout/header.tpl.html'
                    },
                    'nav@app': {
                        templateUrl: 'customer/layout/nav.tpl.html'
                    }
                },
                url: '/customer',
                data: {
                    requiredUserRole: USER_ROLES.customer
                },
                ncyBreadcrumb: {
                    skip: true
                }
            })
            .state('app.customer.error', {
                abstract: true,
                url: '/error'
            })
            .state('app.customer.error.404', {
                url: '/404',
                views: {
                    'content@app': {
                        controller: '404ErrorController'
                    }
                },
                ncyBreadcrumb: {
                    label: '404'
                }
            })
            .state('app.customer.error.403', {
                url: '/403',
                views: {
                    'content@app': {
                        controller: '403ErrorController'
                    }
                },
                ncyBreadcrumb: {
                    label: '403'
                }
            })
            .state('app.customer.error.400', {
                url: '/400',
                views: {
                    'content@app': {
                        controller: '400ErrorController'
                    }
                },
                ncyBreadcrumb: {
                    label: '400'
                }
            })
            .state('app.customer.error.500', {
                url: '/500',
                views: {
                    'content@app': {
                        controller: '500ErrorController'
                    }
                },
                ncyBreadcrumb: {
                    label: '500'
                }
            })
        ;
    }
})();