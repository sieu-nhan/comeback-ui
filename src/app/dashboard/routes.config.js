(function () {
    'use strict';

    angular
        .module('dtag.dashboard')
        .config(addStates)
    ;

    function addStates(UserStateHelperProvider) {
        UserStateHelperProvider.state('dashboard', {
            url: '/dashboard?{startDate}&{endDate}',
            views: {
                'content@app': {
                    controller: 'Dashboard',
                    templateUrl: 'dashboard/dashboard.tpl.html'
                }
            },
            resolve: {
            },
            ncyBreadcrumb: {
                label: 'Dashboard'
            }
        });
    }
})();