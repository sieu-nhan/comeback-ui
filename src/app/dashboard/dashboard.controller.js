(function () {
    'use strict';

    angular
        .module('dtag.dashboard')
        .controller('Dashboard', Dashboard)
    ;

    function Dashboard($scope, TrackingManager) {
        $scope.code = null;
        $scope.checked = null;

        $scope.trackingOrder = trackingOrder;

        function trackingOrder() {
            TrackingManager.one($scope.code).get()
                .then(function(data) {
                    $scope.checked = data;
                });
        }
    }
})();