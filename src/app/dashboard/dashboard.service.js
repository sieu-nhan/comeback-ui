(function(angular) {
    'use strict';

    angular.module('dtag.dashboard')
        .factory('dashboard', dashboard)
    ;

    function dashboard($q, API_REPORT_BASE_URL, dataService, DateFormatter) {
        var api = {
            getDataDashboard: getDataDashboard
        };

        return api;

        /////

        function makeHttpGetRequest(url, params)
        {
            if(!params) {
                params = null;
            }
            else {
                if (!params.startDate) {
                    params.startDate = moment().startOf('month');
                    params.endDate = moment().endOf('month');
                }

                params.startDate = DateFormatter.getFormattedDate(params.startDate);
                params.endDate = DateFormatter.getFormattedDate(params.endDate);
            }

            return dataService.makeHttpGetRequest(url, params, API_REPORT_BASE_URL);
        }

        function getDataDashboard(params) {
            return makeHttpGetRequest('/statistics', params);
        }
    }
})(angular);