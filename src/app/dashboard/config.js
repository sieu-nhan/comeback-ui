(function () {
    'use strict';

    angular
        .module('dtag.dashboard')
        .provider('API_REPORT_BASE_URL', {
            $get: function(API_END_POINT) {
                return API_END_POINT + '/report/v1';
            }
        })
    ;

})();