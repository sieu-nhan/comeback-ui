(function () {
    'use strict';

    angular.module('dtag.core.router', [
        'ui.router'
    ]);
})();