(function () {
    'use strict';

    angular.module('dtag.core.router')
        .config(appConfig)

        .constant('BASE_USER_URLS', {
            admin: '/adm',
            manager: '/manager',
            customer: '/customer'
        })

        .constant('BASE_USER_STATES', {
            admin: 'app.admin',
            manager: 'app.manager',
            customer: 'app.customer'
        })
    ;

    function appConfig($urlRouterProvider) {
        $urlRouterProvider.when('', '/');

        $urlRouterProvider.otherwise(function($injector, $location) {
            var path = $location.path();

            return $injector.invoke(/* @ngInject */ function (Auth, urlPrefixService) {
                if (!Auth.isAuthenticated()) {
                    return '/login';
                }

                if (path === '/') {
                    return urlPrefixService.getPrefixedUrl('/management/orders/list');
                }

                return urlPrefixService.getPrefixedUrl('/error/404');
            });
        });
    }
})();