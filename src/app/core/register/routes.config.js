(function () {
    'use strict';

    angular.module('dtag.core.register')
        .config(function ($stateProvider) {
            $stateProvider
                .state('register', {
                    parent: 'anon',
                    url: '/register',
                    controller: 'Register',
                    templateUrl: 'core/register/register.tpl.html',
                    data: {
                        allowAnonymous: true
                    },
                    resolve: {
                        provinces: function(ProvinceManager) {
                            return ProvinceManager.getList();
                        }
                    }
                })
            ;
        })
    ;
})();