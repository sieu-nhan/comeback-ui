(function () {
    'use strict';

    angular.module('dtag.core.register')
        .controller('Register', Register)
    ;

    function Register($scope, $state, userRestangular, AlertService, ServerErrorProcessor, provinces, RECAPTCHA_KEY) {
        $scope.fieldNameTranslations = {
            username: 'Tên đăng nhập',
            password: 'Mật khẩu',
            email: 'Email'
        };

        $scope.customer = {
            username: null,
            email: null,
            name: null,
            phone: null,
            address: null,
            district: null,
            recaptcha: null
        };

        $scope.formProcessing = false;
        $scope.recaptchaKey = RECAPTCHA_KEY;

        $scope.districts = [];
        $scope.selectData = {
            province: null
        };

        $scope.provinces = provinces;

        if($scope.provinces.length == 1) {
            $scope.selectData = {
                province: $scope.provinces[0].id
            };

            angular.forEach($scope.provinces[0].districts, function(district) {
                if(district.type == 1) {
                    $scope.districts.push(district);
                }
            });
        }

        $scope.selectProvince = function(province) {
            $scope.districts = [];

            angular.forEach(province.districts, function(district) {
                if(district.type == 1) {
                    $scope.districts.push(district);
                }
            });
        };

        $scope.setResponse = function(response) {
            $scope.customer.recaptcha = response;
        };

        $scope.isFormValid = function() {
            return $scope.registerForm.$valid;
        };

        $scope.register = function () {
            var saveUser = userRestangular.one('users').post(null, $scope.customer);

            saveUser
                .catch(
                    function (response) {
                        var errorCheck = ServerErrorProcessor.setFormValidationErrors(response, $scope.registerForm, $scope.fieldNameTranslations);
                        $scope.formProcessing = false;

                        return errorCheck;
                    })
                .then(
                    function () {
                        AlertService.addFlash({
                            type: 'success',
                            message: 'Bạn đã đăng ký thành công, mời đăng nhập!'
                        });

                        $state.go('login');
                    })
            ;
        };
    }
})();