(function () {
    'use strict';

    angular.module('dtag.core.auth')
        .factory('sessionFactory', sessionFactory)
    ;

    function sessionFactory (USER_ROLES) {
        var api = {
            /**
             * @param {String|Object} token
             * @param {Number} id
             * @param {Number} district
             * @param {String} username
             * @param {String} name
             * @param {String} phone
             * @param {String} address
             * @param {Array} [userRoles]
             */
            createNew: function(token, id, username, userRoles, name, phone, district, address) {
                return new Session(token, id, username, userRoles, name, phone, district, address);
            },

            /**
             * @param {Object} data
             */
            createNewFrom: function (data) {
                if (!data.token) {
                    throw new Error('missing token');
                }

                if (!data.id) {
                    throw new Error('missing id');
                }

                if (!data.username) {
                    throw new Error('missing username');
                }

                return this.createNew(data.token, data.id, data.username, data.userRoles, data.name, data.phone, data.district, data.address);
            },

            isSession: function(session) {
                return session instanceof Session;
            }
        };

        function Session(token, id, username, userRoles, name, phone, district, address) {
            this.token = token;
            this.id = parseInt(id, 10) || null;
            this.username = username;

            if (!angular.isArray(userRoles)) {
                userRoles = [];
            }

            this.userRoles = userRoles;
            this.name = name;
            this.phone = phone;
            this.district = district;
            this.address = address;
        }

        Session.prototype.hasUserRole = function(role) {
            return this.userRoles.indexOf(role) !== -1;
        };

        Session.prototype.isAdmin = function() {
            return this.userRoles.indexOf(USER_ROLES.admin) !== -1;
        };

        return api;
    }
})();