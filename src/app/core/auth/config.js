(function () {
    'use strict';

    angular.module('dtag.core.auth')
        .constant('AUTH_TOKEN_NAME', 'dtagToken')
        .constant('PREVIOUS_AUTH_TOKEN_NAME', 'dtagPreviousAuthTokenRaw')

        .constant('USER_ROLES', {
            admin: 'ROLE_ADMIN',
            manager: 'ROLE_MANAGER',
            customer: 'ROLE_CUSTOMER'
        })

        .constant('AUTH_EVENTS', {
            loginSuccess: 'dtag.core.auth.login_success',
            loginFailed: 'dtag.core.auth.login_failed',
            logoutSuccess: 'dtag.core.auth.logout_success',
            sessionTimeout: 'dtag.core.auth.session_timeout',
            notAuthenticated: 'dtag.core.auth.not_authenticated',
            notAuthorized: 'dtag.core.auth.not_authorized'
        })

    ;

})();