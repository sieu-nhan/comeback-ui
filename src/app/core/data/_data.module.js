(function () {
    'use strict';

    angular.module('dtag.core.data', [
        'dtag.core.data.resources'
    ]);
})();