(function () {
    'use strict';

    angular.module('dtag.core.data.resources')
        .factory('userRestangular', userRestangular)
    ;

    function userRestangular (Restangular, API_USER_BASE_URL) {
        return Restangular.withConfig(function (RestangularConfigurer) {
            RestangularConfigurer.setBaseUrl(API_USER_BASE_URL);
        });
    }
})();