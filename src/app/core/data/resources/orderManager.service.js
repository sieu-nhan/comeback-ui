(function () {
    'use strict';

    angular.module('dtag.core.data.resources')
        .factory('OrderManager', OrderManager)
    ;

    function OrderManager (Restangular) {
        var RESOURCE_NAME = 'orders';

        return Restangular.service(RESOURCE_NAME);
    }
})();