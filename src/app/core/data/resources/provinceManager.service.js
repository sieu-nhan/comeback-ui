(function () {
    'use strict';

    angular.module('dtag.core.data.resources')
        .factory('ProvinceManager', ProvinceManager)
    ;

    function ProvinceManager (Restangular) {
        var RESOURCE_NAME = 'provinces';

        return Restangular.service(RESOURCE_NAME);
    }
})();