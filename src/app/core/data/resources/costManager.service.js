(function () {
    'use strict';

    angular.module('dtag.core.data.resources')
        .factory('CostManager', CostManager)
    ;

    function CostManager (Restangular) {
        var RESOURCE_NAME = 'cost';

        return Restangular.service(RESOURCE_NAME);
    }
})();