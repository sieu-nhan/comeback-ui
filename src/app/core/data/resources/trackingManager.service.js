(function () {
    'use strict';

    angular.module('dtag.core.data.resources')
        .factory('TrackingManager', TrackingManager)
    ;

    function TrackingManager (Restangular) {
        var RESOURCE_NAME = 'trackings';

        return Restangular.service(RESOURCE_NAME);
    }
})();