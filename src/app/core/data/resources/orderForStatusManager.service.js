(function () {
    'use strict';

    angular.module('dtag.core.data.resources')
        .factory('OrderForStatusManager', OrderForStatusManager)
    ;

    function OrderForStatusManager (Restangular) {
        var RESOURCE_NAME = 'order';

        return Restangular.service(RESOURCE_NAME);
    }
})();