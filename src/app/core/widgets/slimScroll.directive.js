(function () {
    'use strict';

    angular.module('dtag.core.widgets')
        .directive('slimScroll', slimScroll)
    ;

    function slimScroll() {
        return {
            restrict: 'A',
            link: function(scope, ele, attrs) {
                ele.slimScroll({
                    height: attrs.scrollHeight || '100%'
                })
            }
        }
    }
})();