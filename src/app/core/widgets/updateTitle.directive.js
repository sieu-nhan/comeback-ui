(function () {
    'use strict';

    angular.module('dtag.core.widgets')
        .directive('updateTitle', updateTitle)
    ;

    function updateTitle($rootScope, $window, $translate, $state, $interpolate, _, TOTAL_STORAGE) {
        return {
            link: function(scope, element) {
                var updateTitle = function() {
                    var pageTitle = '';
                    var state = $state.$current;

                    if(_.isObject(state.ncyBreadcrumb) && state.ncyBreadcrumb.label) {
                        var title = state.ncyBreadcrumb.label;
                        title = $interpolate(title)(state.locals.globals);

                        if (title) {
                            pageTitle = title + ' | ';
                        }
                    }

                    if(!!$window.localStorage[TOTAL_STORAGE] && Number($window.localStorage[TOTAL_STORAGE]) > 0) {
                        pageTitle = '(' + $window.localStorage[TOTAL_STORAGE] + ') ' + pageTitle;
                    }

                    pageTitle += $translate.instant('APP_NAME');

                    element.text(pageTitle);
                };

                $rootScope.$on('$stateChangeSuccess', updateTitle);
            }
        };
    }
})();