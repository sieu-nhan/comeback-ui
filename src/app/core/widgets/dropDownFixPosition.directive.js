(function () {
    'use strict';

    angular.module('dtag.core.widgets')
        .directive('dropDownFixPosition', dropDownFixPosition)
    ;

    function dropDownFixPosition($window) {
        return {
            link: function(scope, ele) {
                ele.on('click', function () {
                    dropDownFixPosition(ele, ele.parent().find('.dropdown-menu'));
                });

                angular.element($window).bind('scroll', function () {
                    dropDownFixPosition(ele, ele.parent().find('.dropdown-menu'));
                });

                function dropDownFixPosition(button, dropdown) {
                    var dropDownTop = (button.offset().top + button.outerHeight()) - $(window).scrollTop();
                    var dropDownLeft = button.offset().left;

                    dropdown.css('top', dropDownTop + "px");
                    dropdown.css('left', dropDownLeft + "px");
                }
            }
        };
    }
})();