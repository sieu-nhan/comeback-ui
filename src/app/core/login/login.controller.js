(function () {
    'use strict';

    angular.module('dtag.core.login')
        .controller('Login', Login)
    ;

    function Login($scope, $translate, $stateParams, $rootScope, AUTH_EVENTS, Auth, LANG_KEY) {
        $scope.credentials = {
            username: '',
            password: ''
        };

        if($stateParams.lang == 'vi' || $stateParams.lang == 'en') {
            window.localStorage[LANG_KEY] = $stateParams.lang;
            $translate.use(window.localStorage[LANG_KEY]);
        }

        $scope.lang = window.localStorage[LANG_KEY];
        $scope.frontPageEndPoint = window.location.protocol + '//' + window.location.host.replace('platform.', '');

        $scope.isFormValid = function() {
            return $scope.loginForm.$valid;
        };

        $scope.formProcessing = false;

        $scope.login = function (credentials) {
            if ($scope.formProcessing) {
                return;
            }

            $scope.formProcessing = true;

            Auth.login(credentials, true)
                .then(
                    function () {
                        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                    },
                    function () {
                        $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
                    }
                )
                .finally(function() {
                    $scope.formProcessing = false;
                })
            ;
        };
    }
})();