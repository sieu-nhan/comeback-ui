(function () {
    'use strict';

    angular.module('dtag.core.bootstrap')
        .constant('API_END_POINT', 'http://api.comeback.dev/web/app_dev.php/api')
        .constant('CONNECT_SOCKET', 'http://103.15.50.246:8081')
        .provider('API_BASE_URL', {
            $get: function(API_END_POINT) {
                return API_END_POINT + '/v1';
            }
        })
        .provider('API_USER_BASE_URL', {
            $get: function(API_END_POINT) {
                return API_END_POINT + '/user/v1';
            }
        })
    ;
})();