(function () {
    'use strict';

    var core = angular.module('dtag.core', [
        'ui.router',

        // angular modules

        'ngAnimate',
        'ngSanitize',
        'pascalprecht.translate',
        // cached templates

        'templates-app',
        'templates-common',

        // dtag modules

        'dtag.blocks.alerts',
        'dtag.blocks.serverError',
        'dtag.blocks.errorPage',
        'dtag.blocks.misc',
        'dtag.blocks.util',
        'dtag.blocks.searchBox',
        'dtag.blocks.atSortableQuery',
        'dtag.blocks.event',

        'dtag.core.bootstrap',
        'dtag.core.auth',
        'dtag.core.router',
        'dtag.core.layout',
        'dtag.core.widgets',
        'dtag.core.login',
        'dtag.core.register',
        'dtag.core.resetPassword',
        'dtag.core.data',
        'dtag.core.historyStorage',
        'dtag.core.language',

        // 3rd party modules

        'httpi',
        'underscore',
        'restangular',
        'angular-loading-bar',
        'ui.bootstrap',
        'ui.select',
        'angular-table',
        'daterangepicker',
        'ngJsonExportExcel',
        'angularFileUpload',
        'ui.utils.masks',
        'vcRecaptcha'
    ]);

    core.run(appRun);

    function appRun(Auth, EXISTING_SESSION) {
        // EXISTING_SESSION set by deferred angular bootstrap
        if (EXISTING_SESSION) {
            Auth.initSession(EXISTING_SESSION);
        }
    }
})();