(function () {
    "use strict";

    angular.module("dtag.core.language")
        .constant("LOCALE_VI", {
            "APP_NAME": "Giao vận tận tâm",
            "LOGIN": "Đăng Nhập",
            "LOGOUT": "Đăng Xuất",
            "EDIT_PROFILE": "Sửa hồ sơ Cá nhân",
            "RETURN_TO_ADMIN": "Quay trở lại",
            "PASSWORD": "Mật khẩu",
            "USERNAME": "Tên đăng nhập",
            "FORGOT_PASSWORD": "Quên mật khẩu?",
            "CANCEL": "Hủy bỏ",
            "NO_CANCEL": "Hủy bỏ",
            "YES_DELETE": "Đồng ý",
            "SUBMIT": "Lưu Lại",
            "VALID_FORM": "Nút này sẽ hoạt động nếu tất cả các trường (có dấu *) được điền.",
            "ACTIONS": "Thao tác",
            "CLOSE": "Thoát",
            "ALERTS": "Thông Báo",
            "SEARCH": 'Tím kiếm',
            "ID": "ID",
            "RECORDS_FOUND": 'bản ghi được tìm thấy',
            "HOME_PAGE": "Trang Chủ",
            "STATUS": "Trạng Thái",
            "CONFIRM_BY": "Xác nhận bởi",

            "LAST_UPDATE": "Cập nhật",
            "CREATE_AT": "Ngày tạo",
            "RECEIPT_TYPE": "Loại thu",
            "RECEIPT_ITEM": "Khoản thu",
            "NAME": "Tên",
            "CODE": "Mã",
            "MANAGER": "Quản Lý",
            "DATE_RANGE": "Khoảng thời gian",
            "START_DATE": "Thời gian bắt đầu",
            "END_DATE": "Thời gian kết thúc",
            "DATE": "Thời gian",
            "NEXT": "Tiếp theo",

            "ERROR_PAGE": {
                "400": "Yêu cầu gửi lên máy chủ không hợp lệ",
                "403": "Bạn không có quyền yêu cầu để truy cập này",
                "404": "Các nguồn yêu cầu không thể tìm thấy",
                "500": "Có lỗi xảy ra"
            },
            "EVENT_LISTENER": {
                "LOGIN_FAIL": "Lỗi đăng nhập, vui lòng kiểm tra lại thông tin đăng nhập!",
                "LOGOUT_SUCCESS": "Đăng xuất thành công",
                "SESSION_EXPIRED": "Bạn không được xác thực, phiên làm việc của bạn đã hết hạn, mời đăng nhập lại!"
            },
            "NAV_MODULE": {
                "DASHBOARD": "Dashboard",
                "MANAGERS": "Danh sách Quản Lý",
                "NEW_MANAGER": "Thêm Quản Lý",

                "USERS_MANAGEMENT": "Quản lý người dùng"
            },
            "RESET_PASSWORD_MODULE": {
                "RESET": "Làm mới",
                "USERNAME_EMAIL": "Tên đăng nhập hoặc email",
                "NEW_PASSWORD": "Mật khẩu mới",
                "REPEAT_PASSWORD": "Nhặc lại mật khẩu",
                "RESET_PASSWORD": "Làm mới mật khẩu",

                "SEND_EMAIL_SUCCESS": "Một email đã được gửi tới '{{ username }}'. Trong đó chứa đường dẫn để bạn làm mới mật khẩu của mình",
                "SEND_EMAIL_FAIL": "Không thể làm mới mật khẩu cho '{{ username }}'",
                "RESET_SUCCESS": "Thay đổi thành công, đăng nhập để tiếp tục",
                "TOKEN_NOT_EXISTED": "Thẻ '{{ token }}' không tồn tại",
                "TOKEN_EXPIRED": "Thẻ '{{ token }}' đã hết hạn. Xin vui lòng làm mới lại mật khẩu",
                "INTERNAL_ERROR": "Lỗi cục bộ. Xin vui lòng liên hệ với quản trị viên để được hướng dẫn",

                "HELP_BLOCK_CHECK_EMAIL": "Điền tên đăng nhập hoặc địa chỉ email của bạn cần làm mới. Chúng tôi sẽ gửi cho bạn mội email với tên đăng nhập của bạn và một đường dẫn để bạn làm mới mật khẩu của mình."
            },
            "MANAGER_MODULE": {
                "USERNAME": "Tên đăng nhập",
                "PASSWORD": "Mật khẩu",
                "REPEAT_PASSWORD": "Nhắc lại",
                "COMPANY": "Công ty",
                "EMAIL": "Email",
                "FIRST_NAME": "Tên",
                "LAST_NAME": "Họ và tên đệm",
                "PHONE": "Số điện thoại",
                "ADDRESS": "Địa chỉ",
                "CITY": "Thành phố",
                "STATE": "Bang",
                "COUNTRY": "Quốc gia",
                "ENABLED": "Kích hoạt",
                "GENDER": 'Giới tính',
                "MALE": "Nam",
                "FEMALE": "Nữ",
                "AVATAR": "Hình đại diện",
                "UPLOAD": "Tải lên",
                "INACTIVE": "Ngừng hoạt động",
                "ACTIVE": "Kích hoạt",

                "ADD_NEW_SUCCESS": "Thêm Quản Lý thành công",
                "UPDATE_PROFILE_SUCCESS": "Hồ sơ cá nhân của bạn đã được sửa",
                "UPDATE_STATUS_FAIL": "Không thể thay đổi trang thái",
                "PAUSE_STATUS_SUCCESS": "Quản Lý đã được ngừng hoạt động",
                "ACTIVE_STATUS_SUCCESS": "Quản Lý đã được kích hoạt",
                "CURRENTLY_NO_MANAGER": "Hiện tại không có Quản Lý nào",

                "HELP_BLOCK_REPEAT_PASSWORD": "Để trống nếu bạn không muốn thay đổi mật khẩu.",
                "BACK_TO_MANAGER_LIST": "Quay trở lại danh sách Quản Lý",
                "SELECT_A_MANAGER": "Chọn một Quản Lý",
                "NEW_MANAGER": "Thêm Quản Lý",
                "LOGIN_AS_THIS_MANAGER": "Đăng nhập Quản Lý",
                "LAST_LOGIN": "Đăng nhập lần cuối",
                "STATUS": "Trạng thái",
                "EDIT_MANAGER": "Sửa Quản Lý",
                "DEACTIVATE_MANAGER": "Ngừng hoạt động",
                "ACTIVATE_MANAGER": "Kích hoạt"
            },
            "SHIPPER_MODULE": {
                "ADD_NEW_SUCCESS": "Thêm Shipper thành công",
                "PAUSE_STATUS_SUCCESS": "Shipper đã được ngừng hoạt động",
                "ACTIVE_STATUS_SUCCESS": "Shipper đã được kích hoạt",
                "CURRENTLY_NO_SHIPPER": "Hiện tại không có Shipper nào",

                "BACK_TO_SHIPPER_LIST": "Quay trở lại danh sách Shipper",
                "SELECT_A_SHIPPER": "Chọn một Shipper",
                "NEW_SHIPPER": "Thêm Shipper",
                "LOGIN_AS_THIS_SHIPPER": "Đăng nhập như Shipper",
                "EDIT_SHIPPER": "Sửa Shipper",
                "DEACTIVATE_SHIPPER": "Ngừng hoạt động",
                "ACTIVATE_SHIPPER": "Kích hoạt"
            },
            "REPORT_MODULE": {
                "REPORT_TYPE": "Kiểu báo cáo",
                "SELECT_A_REPORT_TYPE": "Chọn kiểu báo cáo",
                "BREAKDOWN": "Breakdown",
                "OPTION": "Tùy chọn",
                "SELECT_A_OPTION": "Chọn một tùy chọn",
                'GET_REPORT': "Xem báo cáo",

                "TOTAL_COMPLETE": "Tổng đã khám",
                //"TOTAL_IN_COMPLETE": "Total In Complete",
                "TOTAL_EARNING_AMOUNT": "Doanh thu",
                "BILL_TYPE": "Kiểu khám",
                "BILL_VALUE": "Đơn giá",
                "DETAILS": "Chi tiết",
                "REPORT_FAIL": "Có lỗi khi lấy báo cáo",

                "ALL_MANAGER": "Tất Cả Quản Lý",
                "ALL_OWNER": "Tất cả Chủ sở hữu",
                "BY_DAY": "Theo Ngày",
                "BY_REQUEST": "Theo Yêu Cầu",
                "BY_MANAGER": "Theo Quản Lý",
                "BY_OWNER": "Theo Chủ Sở Hữu"
            },
            "DATE_RANGE_PICKER": {
                "TOMORROW": "Ngày mai",
                "TODAY": 'Hôm nay',
                "YESTERDAY": "Hôm qua",
                "LAST7DAYS": "7 ngày trước",
                "LAST30DAYS": "30 ngày trước",
                "THIS_MONTH": "Tháng này",
                "LAST_MONTH": "Tháng trước",
                "LAST_3_MONTH": "3 Tháng trước",
                "LAST_6_MONTH": "6 Tháng trước",
                "THIS_YEAR" : "Năm nay",
                "LAST_YEAR" : "Năm trước",
                "APPLY": "Áp dụng",
                "CANCEL": "Hủy",
                "FROM": "Từ",
                "TO": "Đến",
                "CUSTOM_RANGE": "Tùy chọn",

                "DAY": {
                    "SU": 'CN',
                    "MO": 'T2',
                    "TU": 'T3',
                    "WE": 'T4',
                    "TH": 'T5',
                    "FR": 'T6',
                    "SA": 'T7'
                },

                "MONTH": {
                    "JANUARY": "Tháng 1 ",
                    "FEBRUARY": "Tháng 2 ",
                    "MARCH": "Tháng 3 ",
                    "APRIL": "Tháng 4 ",
                    "MAY": "Tháng 5 ",
                    "JUNE": "Tháng 6 ",
                    "JULY": "Tháng 7 ",
                    "AUGUST": "Tháng 8 ",
                    "SEPTEMBER": "Tháng 9 ",
                    "OCTOBER": "Tháng 10 ",
                    "NOVEMBER": "Tháng 11 ",
                    "DECEMBER": "Tháng 12 "
                },

                "FIRST_DAY": '1'
            },
            "API": {
                "STORE_MODULE": {
                    "STORE_IN_CONTRACT": "Gian hàng đang hoạt động, không thể xóa."
                }
            }
        });
})();