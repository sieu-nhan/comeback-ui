(function () {
    'use strict';

    angular.module('dtag.core.historyStorage')
        .constant('HISTORY', 'dtagHistory')
        .constant('HISTORY_TYPE_PATH', {manager: "manager", shipper: "shipper", order: 'order', customer: "customer"})
    ;
})();