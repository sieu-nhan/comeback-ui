(function () {
    'use strict';

    angular.module('dtag.core.layout', [
        'ui.bootstrap',
        'ui.select'
    ])
        .config(function(uiSelectConfig) {
            uiSelectConfig.theme = 'bootstrap';
        })
    ;
})();