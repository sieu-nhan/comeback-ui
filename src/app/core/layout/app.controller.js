(function () {
    'use strict';

    angular.module('dtag.core.layout')
        .controller('AppController', App)
    ;

    function App($scope, Auth, userSession) {
        $scope.currentUser = userSession;

        $scope.isAdmin = Auth.isAdmin;
        $scope.isCustomer = Auth.isCustomer;

        $scope.admin = {
            layout: 'wide',
            menu: 'horizontal',
            fixedHeader: true,
            fixedSidebar: false
        };
    }
})();