(function () {
    'use strict';

    angular.module('dtag.core')
        .constant('RECAPTCHA_KEY', '6LdcDSoTAAAAAE02Qyg4MOamR9i8IjoWtY1x7A1G')
        .constant('ENTRY_STATE', 'login')
        .constant('TOTAL_STORAGE', 'totalStorage')
        .constant('LIST_ORDER', 'listOrder')
        .config(config)
    ;

    function config($httpProvider) {
        $httpProvider.interceptors.push('authTokenInterceptor');
        $httpProvider.interceptors.push('responseErrorInterceptor');
    }
})();