(function() {
    'use strict';

    angular.module('dtag.management.assignation')
        .controller('AssignationForm', AssignationForm)
    ;

    function AssignationForm($scope, $filter, orders, shippers) {
        $scope.orders = orders;
        $scope.shippers = shippers;

        $scope.assignation = {
            orders: [],
            shipper: null
        };

        $scope.isFormValid = isFormValid;

        function isFormValid() {
            return $scope.assignationForm.$valid && $scope.assignation.orders.length > 0;
        }

        $scope.filename = function() {
            if(isFormValid()) {
                var data = $filter('date')(new Date(), 'dd/MM/yyyy');

                return 'cong viec ' + data + ' - ' + $scope.assignation.shipper.name
            }
        }
    }
})();