(function() {
    'use strict';

    angular.module('dtag.management.assignation')
        .config(addStates)
    ;

    function addStates(UserStateHelperProvider) {
        // uniqueRequestCacheBuster is used as a work-around for reloading only the current state
        // currently UI-Router will reload all parent states as well, this causes problems having

        UserStateHelperProvider
            .state('management.assignation', {
                abstract: true,
                url: '/assignation',
                ncyBreadcrumb: {
                    skip: true
                }
            })
            .state('management.assignation.new', {
                url: '/new',
                views: {
                    'content@app': {
                        controller: 'AssignationForm',
                        templateUrl: 'management/assignation/assignationForm.tpl.html'
                    }
                },
                resolve: {
                    orders: function(OrderForStatusManager) {
                        return OrderForStatusManager.one('shipstatus').getList(null, {status: 1});
                    },
                    shippers: function(ShipperRestangular) {
                        return ShipperRestangular.one('shippers').getList();
                    }
                },
                ncyBreadcrumb: {
                    label: 'Giao việc cho shipper'
                }
            })
        ;
    }
})();