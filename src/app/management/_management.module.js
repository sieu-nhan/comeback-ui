(function() {
    'use strict';

    angular.module('dtag.management', [
        'dtag.management.order',
        'dtag.management.money',
        'dtag.management.assignation',
        'dtag.management.statistics'
    ]);
})();