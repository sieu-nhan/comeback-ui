(function() {
    'use strict';

    angular.module('dtag.management.order')
        .controller('OrderList', OrderList)
    ;

    function OrderList($scope, $modal, $translate, $stateParams, _, $filter, Auth, AlertService, orders, customerList, AtSortableService, OrderForStatusManager, OrderManager, DateFormatter, historyStorage, HISTORY_TYPE_PATH, ORDER_STATUS) {
        $scope.orders = orders;
        $scope.customerList = addAllOption(customerList, ' Tất cả');
        var isCustomer = $scope.isCustomer = Auth.isCustomer();
        $scope.orderStatusToChange = ORDER_STATUS;

        $scope.hasData = function () {
            return !!orders.length;
        };

        if (!$scope.hasData()) {
            AlertService.replaceAlerts({
                type: 'warning',
                message: 'Hiện tại không có vận đơn nào'
            });
        }

        $scope.showPagination = showPagination;
        $scope.getLabelStatus = getLabelStatus;
        $scope.searchOrder = searchOrder;
        $scope.groupEntities = groupEntities;
        $scope.changeStatus = changeStatus;
        $scope.changePaidStatus = changePaidStatus;
        $scope.importOrder = importOrder;
        $scope.getFilename = getFilename;
        $scope.getClassStatus = getClassStatus;

        $scope.orderStatus = addAllOption(angular.copy(ORDER_STATUS), ' Tất cả');

        $scope.tableConfig = {
            itemsPerPage: 100,
            maxPages: 10
        };

        var ranges = {};
        ranges[$translate.instant('DATE_RANGE_PICKER.TODAY')] = [moment().startOf('day'), moment().endOf('day')];
        ranges[$translate.instant('DATE_RANGE_PICKER.YESTERDAY')] = [moment().subtract(1, 'days'), moment().subtract(1, 'days')];
        ranges[$translate.instant('DATE_RANGE_PICKER.LAST7DAYS')] =  [moment().subtract(7, 'days'), moment().endOf('day')];
        ranges[$translate.instant('DATE_RANGE_PICKER.LAST30DAYS')] = [moment().subtract(30, 'days'), moment().endOf('day')];
        ranges[$translate.instant('DATE_RANGE_PICKER.THIS_MONTH')] = [moment().startOf('month'), moment().endOf('month')];
        ranges[$translate.instant('DATE_RANGE_PICKER.LAST_MONTH')] = [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')];

        $scope.datePickerOpts = {
            maxDate:  moment().endOf('day'),
            format: 'DD-MM-YYYY',
            ranges: ranges,
            locale: {
                applyLabel: $translate.instant('DATE_RANGE_PICKER.APPLY'),
                cancelLabel: $translate.instant('DATE_RANGE_PICKER.CANCEL'),
                fromLabel: $translate.instant('DATE_RANGE_PICKER.FROM'),
                toLabel: $translate.instant('DATE_RANGE_PICKER.TO'),
                customRangeLabel: $translate.instant('DATE_RANGE_PICKER.CUSTOM_RANGE'),
                "daysOfWeek": [
                    $translate.instant('DATE_RANGE_PICKER.DAY.SU'),
                    $translate.instant('DATE_RANGE_PICKER.DAY.MO'),
                    $translate.instant('DATE_RANGE_PICKER.DAY.TU'),
                    $translate.instant('DATE_RANGE_PICKER.DAY.WE'),
                    $translate.instant('DATE_RANGE_PICKER.DAY.TH'),
                    $translate.instant('DATE_RANGE_PICKER.DAY.FR'),
                    $translate.instant('DATE_RANGE_PICKER.DAY.SA')
                ],
                "monthNames": [
                    $translate.instant('DATE_RANGE_PICKER.MONTH.JANUARY'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.FEBRUARY'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.MARCH'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.APRIL'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.MAY'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.JUNE'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.JULY'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.AUGUST'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.SEPTEMBER'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.OCTOBER'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.NOVEMBER'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.DECEMBER')
                ],
                "firstDay": Number($translate.instant('DATE_RANGE_PICKER.FIRST_DAY'))
            }
        };

        $scope.selectData = {
            date: {
                startDate: $stateParams.startDate || DateFormatter.getFormattedDate(moment().subtract(7, 'days')),
                endDate: $stateParams.endDate || DateFormatter.getFormattedDate(moment().startOf('days'))
            },
            status: null,
            customerId: $stateParams.customerId || null
        };

        $scope.confirmDeletion = function (order) {
            var modalInstance = $modal.open({
                templateUrl: 'management/order/confirmDeletion.tpl.html'
            });

            modalInstance.result.then(function () {
                return OrderManager.one(order.id).remove()
                    .then(
                        function () {
                            var index = orders.indexOf(order);

                            if (index > -1) {
                                orders.splice(index, 1);
                            }

                            $scope.orders = orders;

                            if($scope.tableConfig.currentPage > 0 && orders.length/10 == $scope.tableConfig.currentPage) {
                                AtSortableService.insertParamForUrl({page: $scope.tableConfig.currentPage});
                            }

                            AlertService.replaceAlerts({
                                type: 'success',
                                message: 'Xóa vận đợn thành công'
                            });
                    })
                    .catch(function () {
                        AlertService.replaceAlerts({
                            type: 'danger',
                            message: 'Xóa vận đơn thất bại'
                        });
                    })
                ;
            });
        };

        /**
         *
         * @param {Array} data
         * @param {String} [label]
         * @returns {Array}
         */
        function addAllOption(data, label) {
            if (!angular.isArray(data)) {
                throw new Error('Expected an array of data');
            }

            data.unshift({
                id: null,
                key: null, // default value
                label: label || 'All',
                username: label || 'All'
            });

            return data;
        }

        function showPagination() {
            return angular.isArray($scope.orders) && $scope.orders.length > $scope.tableConfig.itemsPerPage;
        }

        function getLabelStatus(key) {
            var status = _.find(ORDER_STATUS, function(st) {
                return key == st.key;
            });

            if(!angular.isObject(status)) {
                throw('status null');
            }

            return status.label;
        }

        function groupEntities(item){
            if (item.id === null) {
                return undefined; // no group
            }

            return ''; // separate group with no name
        }

        function searchOrder() {
            var params = {
                status: $scope.selectData.status,
                startDate: DateFormatter.getFormattedDate($scope.selectData.date.startDate),
                endDate: DateFormatter.getFormattedDate($scope.selectData.date.endDate),
                customerId: $scope.selectData.customerId
            };

            OrderForStatusManager.one('shipstatus').getList(null, params)
                .then(function(ordersList) {
                    $scope.orders = orders = ordersList.plain();
                });
        }

        function importOrder() {
            $modal.open({
                templateUrl: 'management/order/importOrder.tpl.html',
                size: 'lg',
                keyboard: false,
                backdrop: 'static',
                controller: 'ImportOrder'
            });
        }

        function getFilename() {
            return 'Danh sách đơn hàng ' + $filter('date')(new Date(), 'dd-MM-yyyy');
        }

        function getClassStatus(key) {
            switch(key) {
                case 0:
                    return 'btn-danger';
                    break;
                case 1:
                    return 'btn-warning';
                    break;
                case 2:
                    return 'btn-info';
                    break;
                case 3:
                    return 'btn-primary';
                    break;
                case 4:
                    return 'btn-brown';
                    break;
                case 5:
                    return 'btn-success';
                    break;
                case 6:
                    return 'btn-blue';
                    break;
            }

            return 'btn-default';
        }

        function changePaidStatus(status, order) {
            OrderManager.one(order.id).patch({paidStatus: status})
                .catch(function () {
                    AlertService.replaceAlerts({
                        type: 'error',
                        message: 'Thay đổi trạng thái thất bại'
                    });
                })
                .then(function () {
                    order.paidStatus = status;

                    AlertService.replaceAlerts({
                        type: 'success',
                        message: 'Thay đổi trạng thái thành công'
                    });
                })
        }

        function changeStatus(status, order) {
            OrderManager.one(order.id).patch({shippingStatus: status})
                .catch(function () {
                    AlertService.replaceAlerts({
                        type: 'error',
                        message: 'Thay đổi trạng thái thất bại'
                    });
                })
                .then(function () {
                    order.shippingStatus = status;

                    AlertService.replaceAlerts({
                        type: 'success',
                        message: 'Thay đổi trạng thái thành công'
                    });
                })

        }

        $scope.$on('$locationChangeSuccess', function() {
            historyStorage.setParamsHistoryCurrent(HISTORY_TYPE_PATH.order)
        });
    }
})();