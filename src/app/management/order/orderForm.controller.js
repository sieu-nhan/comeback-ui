(function() {
    'use strict';

    angular.module('dtag.management.order')
        .controller('OrderForm', OrderForm)
    ;

    function OrderForm($scope, provinces, order, OrderManager, CostManager, Auth, historyStorage, ServerErrorProcessor, AlertService, HISTORY_TYPE_PATH, ORDER_STATUS, ORDER_PAY_STATUS) {
        $scope.fieldNameTranslations = {
            orderId: 'Mã đơn hàng',
            senderName: 'Tên người gửi',
            senderAddress: 'Địa chỉ người gửi',
            receiverName: 'Tên người nhận',
            receiverAddress: 'Địa chỉ người nhận',
            receiverPhone: 'Số điện thoại người nhận',
            senderPhone: 'Số điện thoại người gửi',
            targetProvince: 'Tỉnh đến',
            description: 'Mô tả',
            cod: 'Tiền thu hộ',
            cost: 'Giá tiền',
            weight: 'Trọng lượng',
            province: 'Tỉnh đi',
            district: 'Huyện đi',
            shippingStatus: 'shippingStatus'
        };

        var timeoutCosting = null;
        var isCustomer = $scope.isCustomer = Auth.isCustomer();
        var userSession = Auth.getSession();

        $scope.order = order || {
            orderId: null,
            senderName: isCustomer ? userSession.name : null,
            senderAddress: isCustomer ? userSession.address : null,
            senderPhone: isCustomer ? userSession.phone : null,
            senderDistrict: isCustomer ? !!userSession.district ? userSession.district.id : null : null,
            receiverName:null,
            receiverAddress: null,
            receiverPhone: null,
            targetDistrict: null,
            description: null,
            weight: null,
            width: null,
            height: null,
            length: null,
            urgent: false,
            cod: 0,
            senderPay: true,
            shippingStatus: 0,
            paidStatus: false
        };

        $scope.districts = [];
        $scope.targetDistricts = [];
        $scope.orderStatus = ORDER_STATUS;
        $scope.payStatus = ORDER_PAY_STATUS;
        $scope.provinces =  provinces;
        $scope.isNew = order === null;
        $scope.formProcessing = false;
        $scope.optionCost = [];

        $scope.optionSenderPay = [
            {label: 'Người Nhận', key: true},
            {label: 'Người Gửi', key: false}
        ];

        $scope.selectData = {
            senderProvince: !$scope.isNew ? order.senderDistrict.province.id : null,
            targetProvince: !$scope.isNew ? order.targetDistrict.province.id :  null
        };

        if($scope.provinces.length == 1) {
            $scope.selectData = {
                senderProvince: $scope.provinces[0].id,
                targetProvince: $scope.provinces[0].id
            };

            $scope.districts = [];

            angular.forEach($scope.provinces[0].districts, function(district) {
                if(district.type == 1) {
                    $scope.districts.push(district);
                }
            });

            $scope.targetDistricts = $scope.provinces[0].districts;
        }

        if(!$scope.isNew) {
            autoCost();
        }

        $scope.backToListOrder = backToListOrder;
        $scope.selectProvince = selectProvince;
        $scope.selectTargetProvince = selectTargetProvince;
        $scope.autoCost = autoCost;

        $scope.isFormValid = function() {
            return $scope.orderForm.$valid && typeof $scope.optionCost.distance == 'number';
        };

        function backToListOrder() {
            return historyStorage.getLocationPath(HISTORY_TYPE_PATH.order, '^.list');
        }

        function selectProvince(province) {
            $scope.order.senderDistrict = null;

            $scope.districts = [];

            angular.forEach(province.districts, function(district) {
                if(district.type == 1) {
                    $scope.districts.push(district);
                }
            });
        }

        function selectTargetProvince(province) {
            $scope.order.targetDistrict = null;

            $scope.targetDistricts = province.districts;
        }

        function autoCost() {
            if(!$scope.order.senderDistrict || !$scope.order.senderAddress || !$scope.order.targetDistrict || !$scope.order.receiverAddress || !$scope.order.weight) {
                return;
            }

            var params = {
                senderDistrict: $scope.order.senderDistrict.id || $scope.order.senderDistrict,
                senderAddress: $scope.order.senderAddress,
                targetDistrict: $scope.order.targetDistrict.id || $scope.order.targetDistrict,
                receiverAddress: $scope.order.receiverAddress,
                weight: $scope.order.weight
            };

            clearTimeout(timeoutCosting);

            timeoutCosting = setTimeout(function() {
                CostManager.one('autocost').get(params)
                    .then(function(data) {
                        $scope.optionCost = data;
                    })
                    .catch(function() {
                        $scope.optionCost = [];
                    })
            }, 500)
        }

        $scope.submit = function(urgent) {
            if ($scope.formProcessing) {
                // already running, prevent duplicates
                return;
            }

            $scope.formProcessing = true;

            if(typeof urgent == 'boolean') {
                $scope.order.urgent = urgent;
            }

            delete $scope.order.deletedAt;
            delete $scope.order.updatedAt;
            delete $scope.order.createdAt;
            delete $scope.order.creator;
            delete $scope.order.totalCost;
            delete $scope.order.shippingCost;
            delete $scope.order.senderIncome;
            delete $scope.order.codStatus;
            delete $scope.order.goodType;
            delete $scope.order.distance;

            if(isCustomer) {
                delete $scope.order.paidStatus;
                delete $scope.order.shippingStatus;
            }

            var saveStore = $scope.isNew ? OrderManager.post($scope.order) : $scope.order.patch();

            saveStore
                .catch(
                    function (response) {
                        var errorCheck = ServerErrorProcessor.setFormValidationErrors(response, $scope.orderForm, $scope.fieldNameTranslations);
                        $scope.formProcessing = false;

                        return errorCheck;
                    }
                )
                .then(
                    function () {
                        AlertService.addFlash({
                            type: 'success',
                            message: $scope.isNew ? 'Thêm mới vận đơn thành công' : 'Sửa vận đơn thành công'
                        });
                    }
                )
                .then(
                    function () {
                        return historyStorage.getLocationPath(HISTORY_TYPE_PATH.order, '^.list');
                    }
                )
            ;
        };
    }
})();