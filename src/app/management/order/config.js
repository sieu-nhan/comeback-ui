(function () {
    'use strict';

    angular.module('dtag.core')
        .constant('ORDER_STATUS', [
            {key: 0, label: 'Tạo vận đơn'},
            {key: 1, label: 'Đã Nhận vận đơn'},
            {key: 2, label: 'Đang giao'},
            {key: 3, label: 'Hoàn lại'},
            {key: 4, label: 'Phát lại'},
            {key: 5, label: 'Gửi thành công'},
            {key: 6, label: 'Hủy đơn hàng'}
        ])
        .constant('ORDER_PAY_STATUS', [
            {key: 0, label: 'Chưa thanh toán'},
            {key: 1, label: 'Đã thanh toán'}
        ])
    ;
})();