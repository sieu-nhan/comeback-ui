(function() {
    'use strict';

    angular.module('dtag.management.order')
        .controller('ImportOrder', ImportOrder)
    ;

    function ImportOrder($scope, $state, sessionStorage, FileUploader, API_END_POINT) {
        var uploader = $scope.uploader = new FileUploader({
            filters: [{
                fn: function(item) {
                    //if(item.type != "text/csv") {
                    //    return false;
                    //}

                    return true
                }
            }],

            url: API_END_POINT+'/v1/orders/excels',
            headers: {
                Authorization: 'Bearer ' + sessionStorage.getCurrentToken()
            },
            autoUpload: true
        });

        uploader.onErrorItem = function() {
            console.info('upload file error.');
        };

        uploader.onSuccessItem = function(item, response, status, headers) {
            $state.reload();
        };
    }
})();