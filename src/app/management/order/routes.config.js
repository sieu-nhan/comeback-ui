(function() {
    'use strict';

    angular.module('dtag.management.order')
        .config(addStates)
    ;

    function addStates(UserStateHelperProvider) {
        // uniqueRequestCacheBuster is used as a work-around for reloading only the current state
        // currently UI-Router will reload all parent states as well, this causes problems having

        UserStateHelperProvider
            .state('management.order', {
                abstract: true,
                url: '/orders',
                ncyBreadcrumb: {
                    skip: true
                }
            })
            .state('management.order.list', {
                url: '/list?page&sortField&orderBy&search&startDate&endDate&customerId&status',
                params: {
                    uniqueRequestCacheBuster: null
                },
                views: {
                    'content@app': {
                        controller: 'OrderList',
                        templateUrl: 'management/order/orderList.tpl.html'
                    }
                },
                resolve: {
                    orders: /* @ngInject */ function(OrderForStatusManager, $stateParams, DateFormatter) {
                        var params = {
                            startDate: $stateParams.startDate || DateFormatter.getFormattedDate(moment().subtract(7, 'days')),
                            endDate: $stateParams.endDate || DateFormatter.getFormattedDate(moment().startOf('days')),
                            customerId: $stateParams.customerId,
                            status: $stateParams.status
                        };

                        return OrderForStatusManager.one('shipstatus').getList(null, params)
                    },
                    customerList: function(customerRestangular, Auth) {
                        if(Auth.isCustomer()) {
                            return []
                        }

                        return customerRestangular.one('customers', 'all').getList();
                    }
                },
                ncyBreadcrumb: {
                    label: 'Danh sách vận đơn'
                }

            })
            .state('management.order.new', {
                url: '/new',
                views: {
                    'content@app': {
                        controller: 'OrderForm',
                        templateUrl: 'management/order/orderForm.tpl.html'
                    }
                },
                resolve: {
                    order: function() {
                        return null;
                    },
                    provinces: function(ProvinceManager) {
                        return ProvinceManager.getList();
                    }
                },
                ncyBreadcrumb: {
                    label: 'Thêm mới vận đơn'
                }
            })
            .state('management.order.edit', {
                url: '/edit/{id:[0-9]+}',
                views: {
                    'content@app': {
                        controller: 'OrderForm',
                        templateUrl: 'management/order/orderForm.tpl.html'
                    }
                },
                resolve: {
                    order: /* @ngInject */ function(OrderManager, $stateParams) {
                        return OrderManager.one($stateParams.id).get();
                    },
                    provinces: function(ProvinceManager) {
                        return ProvinceManager.getList();
                    }
                },
                ncyBreadcrumb: {
                    label: 'Sửa vận đơn'
                }
            })
        ;
    }
})();