(function() {
    'use strict';

    angular.module('dtag.management')
        .config(addStates)
    ;

    function addStates(UserStateHelperProvider) {
        UserStateHelperProvider
            .state('management', {
                abstract: true,
                url: '/management',
                ncyBreadcrumb: {
                    label: 'Management'
                }
            })
        ;
    }
})();