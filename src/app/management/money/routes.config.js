(function() {
    'use strict';

    angular.module('dtag.management.money')
        .config(addStates)
    ;

    function addStates(UserStateHelperProvider) {
        // uniqueRequestCacheBuster is used as a work-around for reloading only the current state
        // currently UI-Router will reload all parent states as well, this causes problems having

        UserStateHelperProvider
            .state('management.money', {
                abstract: true,
                url: '/money',
                ncyBreadcrumb: {
                    skip: true
                }
            })
            .state('management.money.list', {
                url: '/list?customerId',
                views: {
                    'content@app': {
                        controller: 'MoneyList',
                        templateUrl: 'management/money/moneyList.tpl.html'
                    }
                },
                resolve: {
                    money: /* @ngInject */ function() {
                        return []
                    },
                    customerList: function(customerRestangular, Auth) {
                        if(Auth.isCustomer()) {
                            return []
                        }

                        return customerRestangular.one('customers', 'all').getList()
                            .then(function(customers) {
                                return customers.plain()
                            });
                    },
                    report: function(userSession, API_REPORTS_BASE_URL, dataService, $stateParams, Auth) {
                        var params = {
                            customer: null
                        };

                        if(!Auth.isCustomer()) {
                            params.customerId = $stateParams.customerId;
                        }

                        return dataService.makeHttpGetRequest('/managermoney', params, API_REPORTS_BASE_URL)
                            .catch(function() {
                                return false
                        });
                    }
                },
                ncyBreadcrumb: {
                    label: 'Quản lý tiền thu hộ'
                },
                reloadOnSearch: false
            })
        ;
    }
})();