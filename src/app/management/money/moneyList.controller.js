(function() {
    'use strict';

    angular.module('dtag.management.money')
        .controller('MoneyList', MoneyList)
    ;

    function MoneyList($scope, $location, $stateParams, money, customerList, report, Auth, dataService, API_REPORTS_BASE_URL) {
        $scope.money = money;
        $scope.report = report;

        $scope.customerList = addAllOption(customerList, ' Tất cả khách hàng');

        $scope.selectData = {
            customer: $stateParams.customerId || null
        };

        $scope.groupEntities = groupEntities;
        $scope.searchForCustomer = searchForCustomer;

        function groupEntities(item){
            if (item.id === null) {
                return undefined; // no group
            }

            return ''; // separate group with no name
        }

        /**
         *
         * @param {Array} data
         * @param {String} [label]
         * @returns {Array}
         */
        function addAllOption(data, label) {
            if (!angular.isArray(data)) {
                throw new Error('Expected an array of data');
            }

            data.unshift({
                id: null, // default value
                username: label || 'All'
            });

            return data;
        }

        function searchForCustomer(customer) {
            var params = {
                customerId: null
            };

            if(!Auth.isCustomer()) {
                params.customerId = customer;
            }

            dataService.makeHttpGetRequest('/managermoney', params, API_REPORTS_BASE_URL)
                .then(function(report) {
                    $scope.report = report;

                    $location.search({ customerId: params.customer });
                })
                .catch(function() {
                    return false
                });
        }
    }
})();