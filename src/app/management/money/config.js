(function() {
    'use strict';

    angular.module('dtag.management.money')
        .provider('API_REPORTS_BASE_URL', {
            $get: function(API_END_POINT) {
                return API_END_POINT + '/report/v1';
            }
        })
    ;
})();