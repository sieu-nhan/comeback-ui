(function() {
    'use strict';

    angular.module('dtag.management.statistics')
        .controller('StatisticsList', StatisticsList)
    ;

    function StatisticsList($scope, _, $translate, $stateParams, reports, OrderForStatusManager, DateFormatter) {
        $scope.reports = _refactorJson(reports);

        var ranges = {};
        ranges[$translate.instant('DATE_RANGE_PICKER.TODAY')] = [moment().startOf('day'), moment().endOf('day')];
        ranges[$translate.instant('DATE_RANGE_PICKER.YESTERDAY')] = [moment().subtract(1, 'days'), moment().subtract(1, 'days')];
        ranges[$translate.instant('DATE_RANGE_PICKER.LAST7DAYS')] =  [moment().subtract(7, 'days'), moment().endOf('day')];
        ranges[$translate.instant('DATE_RANGE_PICKER.LAST30DAYS')] = [moment().subtract(30, 'days'), moment().endOf('day')];
        ranges[$translate.instant('DATE_RANGE_PICKER.THIS_MONTH')] = [moment().startOf('month'), moment().endOf('month')];
        ranges[$translate.instant('DATE_RANGE_PICKER.LAST_MONTH')] = [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')];

        $scope.datePickerOpts = {
            maxDate:  moment().endOf('day'),
            format: 'DD-MM-YYYY',
            ranges: ranges,
            locale: {
                applyLabel: $translate.instant('DATE_RANGE_PICKER.APPLY'),
                cancelLabel: $translate.instant('DATE_RANGE_PICKER.CANCEL'),
                fromLabel: $translate.instant('DATE_RANGE_PICKER.FROM'),
                toLabel: $translate.instant('DATE_RANGE_PICKER.TO'),
                customRangeLabel: $translate.instant('DATE_RANGE_PICKER.CUSTOM_RANGE'),
                "daysOfWeek": [
                    $translate.instant('DATE_RANGE_PICKER.DAY.SU'),
                    $translate.instant('DATE_RANGE_PICKER.DAY.MO'),
                    $translate.instant('DATE_RANGE_PICKER.DAY.TU'),
                    $translate.instant('DATE_RANGE_PICKER.DAY.WE'),
                    $translate.instant('DATE_RANGE_PICKER.DAY.TH'),
                    $translate.instant('DATE_RANGE_PICKER.DAY.FR'),
                    $translate.instant('DATE_RANGE_PICKER.DAY.SA')
                ],
                "monthNames": [
                    $translate.instant('DATE_RANGE_PICKER.MONTH.JANUARY'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.FEBRUARY'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.MARCH'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.APRIL'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.MAY'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.JUNE'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.JULY'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.AUGUST'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.SEPTEMBER'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.OCTOBER'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.NOVEMBER'),
                    $translate.instant('DATE_RANGE_PICKER.MONTH.DECEMBER')
                ],
                "firstDay": Number($translate.instant('DATE_RANGE_PICKER.FIRST_DAY'))
            }
        };

        $scope.selectData = {
            date: {
                startDate: $stateParams.startDate || DateFormatter.getFormattedDate(moment().startOf('days')),
                endDate: $stateParams.endDate || DateFormatter.getFormattedDate(moment().startOf('days'))
            }
        };

        $scope.generateReport = generateReport;

        function generateReport() {
            var params = {
                startDate: DateFormatter.getFormattedDate($scope.selectData.date.startDate),
                endDate: DateFormatter.getFormattedDate($scope.selectData.date.endDate)
            };

            OrderForStatusManager.one('shipstatus').getList(null, params)
                .then(function(reports) {
                    $scope.reports = _refactorJson(reports.plain());
                });
        }

        function _refactorJson(data) {
            var reports = [];

            angular.forEach(data, function(value) {
                if(!angular.isObject(value) || !angular.isObject(value.creator)) {
                    return
                }

                var reportIndex = _.findIndex(reports, function(report) {
                    return report.creator.id == value.creator.id
                });

                if(reportIndex > -1) {
                    reports[reportIndex].number++;
                    reports[reportIndex].totalCost = reports[reportIndex].totalCost + value.totalCost;
                    reports[reportIndex].shippingCost = reports[reportIndex].shippingCost + value.shippingCost;
                } else {
                    var newIndex = reports.length;

                    reports[newIndex] = value;
                    reports[newIndex].number = 1;
                }
            });

            return reports;
        }
    }
})();