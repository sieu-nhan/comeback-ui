(function() {
    'use strict';

    angular.module('dtag.management.statistics')
        .config(addStates)
    ;

    function addStates(UserStateHelperProvider) {
        // uniqueRequestCacheBuster is used as a work-around for reloading only the current state
        // currently UI-Router will reload all parent states as well, this causes problems having

        UserStateHelperProvider
            .state('management.statistics', {
                abstract: true,
                url: '/statistics',
                ncyBreadcrumb: {
                    skip: true
                }
            })
            .state('management.statistics.list', {
                url: '/list?page&sortField&orderBy&search&startDate&endDate&customerId&status',
                views: {
                    'content@app': {
                        controller: 'StatisticsList',
                        templateUrl: 'management/statistics/statisticsList.tpl.html'
                    }
                },
                resolve: {
                    reports: /* @ngInject */ function(OrderForStatusManager, $stateParams, DateFormatter) {
                        var params = {
                            startDate: $stateParams.startDate || DateFormatter.getFormattedDate(moment().startOf('days')),
                            endDate: $stateParams.endDate || DateFormatter.getFormattedDate(moment().startOf('days')),
                            customerId: $stateParams.customerId,
                            status: $stateParams.status
                        };

                        return OrderForStatusManager.one('shipstatus').getList(null, params)
                    }
                },
                ncyBreadcrumb: {
                    label: 'Thống Kê'
                },
                reloadOnSearch: false
            })
        ;
    }
})();